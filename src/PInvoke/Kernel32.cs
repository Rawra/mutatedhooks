﻿using MutatedHooks.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks.PInvoke
{
    public unsafe static class Kernel32
    {
        public const uint PAGE_EXECUTE_READWRITE = 0x40;
        public const uint MEM_COMMIT = 0x1000;
        public const uint MEM_RELEASE = 0x8000;
        public const string KERNELDLL_NAME = "kernel32.dll";

        [DllImport(KERNELDLL_NAME, CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true, EntryPoint = "GetProcAddress")]
        public static extern IntPtr GetProcAddressSafe(IntPtr hModule, string procName);


        /*[DllImport(KERNELDLL_NAME, SetLastError = true)]
        public static extern nint VirtualAlloc(nint lpAddress, uint dwSize, uint flAllocationType, uint flProtect);

        [DllImport(KERNELDLL_NAME, SetLastError = true)]
        public static extern bool VirtualFree(nint lpAddress, uint dwSize, uint dwFreeType);
        
        [DllImport(KERNELDLL_NAME, SetLastError = true)]
        public static extern bool VirtualProtectEx(nint hProcess, nint lpAddress, uint dwSize, uint flNewProtect, out uint lpflOldProtect);*/

#if NET8_0
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, byte*, UInt32, UInt32*, bool> WriteProcessMemory;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, byte*, UInt32, UInt32*, bool> ReadProcessMemory;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, UInt32, UInt32, IntPtr > VirtualAlloc;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, UInt32, UInt32, IntPtr> VirtualAllocEx;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, UInt32, bool> VirtualFree;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, UInt32, UInt32*, bool> VirtualProtectEx;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, UInt32, UInt32*, bool> VirtualProtect;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, IntPtr> GetProcAddress;

        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr> LoadLibraryA;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr> LoadLibraryW;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, IntPtr> LoadLibraryExA;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, IntPtr> LoadLibraryExW;
        public static delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, bool> FreeLibrary;
#else
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr, byte*, UInt32, UInt32*, bool> WriteProcessMemory;
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr, byte*, UInt32, UInt32*, bool> ReadProcessMemory;
        public static delegate* unmanaged[Stdcall]<IntPtr, UInt32, UInt32, UInt32, IntPtr > VirtualAlloc;
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, UInt32, UInt32, IntPtr> VirtualAllocEx;
        public static delegate* unmanaged[Stdcall]<IntPtr, UInt32, UInt32, bool> VirtualFree;
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, UInt32, UInt32*, bool> VirtualProtectEx;
        public static delegate* unmanaged[Stdcall]<IntPtr, UInt32, UInt32, UInt32*, bool> VirtualProtect;
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr, IntPtr> GetProcAddress;

        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr> LoadLibraryA;
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr> LoadLibraryW;
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, IntPtr> LoadLibraryExA;
        public static delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, IntPtr> LoadLibraryExW;
        public static delegate* unmanaged[Stdcall]<IntPtr, bool> FreeLibrary;
#endif
        static Kernel32()
        {
            if (!ModuleHelper.TryGetModuleBaseAddress(KERNELDLL_NAME, out IntPtr kernelbase))
                throw new Exception("could not find kernel32 image base");

#if NET8_0
            GetProcAddress = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, IntPtr>)NativeLibrary.GetExport(kernelbase, "GetProcAddress");
            WriteProcessMemory = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, byte*, UInt32, UInt32*, bool>)NativeLibrary.GetExport(kernelbase, "WriteProcessMemory");
            ReadProcessMemory = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, byte*, UInt32, UInt32*, bool>)NativeLibrary.GetExport(kernelbase, "ReadProcessMemory");
            VirtualAlloc = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, UInt32, UInt32, IntPtr>)NativeLibrary.GetExport(kernelbase, "VirtualAlloc");
            VirtualAllocEx = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, UInt32, UInt32, IntPtr>)NativeLibrary.GetExport(kernelbase, "VirtualAllocEx");
            VirtualFree = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, UInt32, bool>)NativeLibrary.GetExport(kernelbase, "VirtualFree");

            VirtualProtect = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, UInt32, UInt32, UInt32*, bool>)NativeLibrary.GetExport(kernelbase, "VirtualProtect");
            VirtualProtectEx = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, UInt32, UInt32*, bool>)NativeLibrary.GetExport(kernelbase, "VirtualProtectEx");

            LoadLibraryA = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr>)NativeLibrary.GetExport(kernelbase, "LoadLibraryA");
            LoadLibraryW = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr>)NativeLibrary.GetExport(kernelbase, "LoadLibraryW");
            LoadLibraryExA = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, IntPtr>)NativeLibrary.GetExport(kernelbase, "LoadLibraryExA");
            LoadLibraryExW = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, IntPtr, UInt32, IntPtr>)NativeLibrary.GetExport(kernelbase, "LoadLibraryExW");
            FreeLibrary = (delegate* unmanaged[Stdcall, SuppressGCTransition]<IntPtr, bool>)NativeLibrary.GetExport(kernelbase, "FreeLibrary");
#else
            GetProcAddress = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, IntPtr>)GetProcAddressSafe(kernelbase, "GetProcAddress");
            WriteProcessMemory = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, byte*, UInt32, UInt32*, bool>)GetProcAddressSafe(kernelbase, "WriteProcessMemory");
            ReadProcessMemory = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, byte*, UInt32, UInt32*, bool>)GetProcAddressSafe(kernelbase, "ReadProcessMemory");
            VirtualAlloc = (delegate* unmanaged[Stdcall]<IntPtr, UInt32, UInt32, UInt32, IntPtr>)GetProcAddressSafe(kernelbase, "VirtualAlloc");
            VirtualAllocEx = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, UInt32, UInt32, IntPtr>)GetProcAddressSafe(kernelbase, "VirtualAllocEx");
            VirtualFree = (delegate* unmanaged[Stdcall]<IntPtr, UInt32, UInt32, bool>)GetProcAddressSafe(kernelbase, "VirtualFree");

            VirtualProtect = (delegate* unmanaged[Stdcall]<IntPtr, UInt32, UInt32, UInt32*, bool>)GetProcAddressSafe(kernelbase, "VirtualProtect");
            VirtualProtectEx = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, UInt32, UInt32*, bool>)GetProcAddressSafe(kernelbase, "VirtualProtectEx");

            LoadLibraryA = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr>)GetProcAddressSafe(kernelbase, "LoadLibraryA");
            LoadLibraryW = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr>)GetProcAddressSafe(kernelbase, "LoadLibraryW");
            LoadLibraryExA = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, IntPtr>)GetProcAddressSafe(kernelbase, "LoadLibraryExA");
            LoadLibraryExW = (delegate* unmanaged[Stdcall]<IntPtr, IntPtr, UInt32, IntPtr>)GetProcAddressSafe(kernelbase, "LoadLibraryExW");

            FreeLibrary = (delegate* unmanaged[Stdcall]<IntPtr, bool>)GetProcAddressSafe(kernelbase, "FreeLibrary");
#endif
        }

    }
}
