﻿using Iced.Intel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks.ManagedInlineHook
{
    /// <summary>
    /// Memory code reader for Iced's decoder to read
    /// from any arbitrary memory location via pointers.
    /// </summary>
    internal unsafe sealed class MemoryCodeReader : CodeReader
    {
        public byte* data;
        public int length;
        private int currentPosition = 0;

        public MemoryCodeReader(byte* data, int length)
        {
            this.data = data;
            this.length = length;
        }

        public MemoryCodeReader(ReadOnlySpan<byte> data)
        {
            fixed (byte* ptr = data)
            {
                this.data = ptr;
            }
            this.length = data.Length;
        }

        public override int ReadByte()
        {
            if (currentPosition >= length)
                return -1;
            return data[currentPosition++];
        }
    }
}
