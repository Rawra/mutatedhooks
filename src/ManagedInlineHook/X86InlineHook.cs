﻿using Iced.Intel;
using MutatedHooks.Extensions;
using MutatedHooks.ManagedInlineHook;
using MutatedHooks.PInvoke;
using MutatedHooks.Utility;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace MutatedHooks
{
    /// <summary>
    /// Specialized Inline hooking class
    /// Can be used to detour code execution via jmp
    /// to a user-defined assembler code stub. 
    /// 
    /// Which in turn can be used to interop with C# code.
    /// 
    /// A more user-friendly API is not planned due to the very specialized nature of
    /// such a hook, and allowing the user to define custom asm will greatly
    /// help the flexibility of this class.
    /// 
    /// ToDo: Add a "offset" argument that makes the modifies the jump back target address
    /// </summary>
    public unsafe sealed class X86InlineHook : IDisposable
    {
        private Assembler stubAsm;
        public IntPtr stubAddressVA;
        public IntPtr targetAddressVA;

        public IntPtr jumpBackAddressVA;
        private bool dropOverwrittenInstructions;
        private IntPtr jumpBackOffset;

        private Instruction[]? overwrittenInstructions;
        private SharedMemory.PageAllocationSegment pageAllocationSegment;

        /// <summary>
        /// Create a mid-function hook at address, generate a user assembly stub and jmp to it, then return to original control flow.
        /// </summary>
        /// <param name="addressVA">The place where to put the hook</param>
        /// <param name="stubAsm">The user-defined assembler that contains the stub code content</param>
        /// <param name="assemblerBitness">Optional bitness used by the assembler</param>
        /// <param name="callsiteJmpHookSize">The size of the callsite jmp hook (Must at least be able to contain the jmp instruction + operand; all overwritten values get restored at the stub epilogue)</param>
        /// <param name="dropOverwrittenInstructions">Drop any overwritten instructions at address</param>
        public X86InlineHook(IntPtr addressVA, out Assembler stubAsm, int assemblerBitness = 32, IntPtr jumpBackOffset = default(IntPtr), bool dropOverwrittenInstructions = false, IntPtr jumpBackAddressVA = default(IntPtr))
        {
            this.stubAsm = new Assembler(assemblerBitness);
            stubAsm = this.stubAsm;
            this.targetAddressVA = addressVA;
            this.jumpBackOffset = jumpBackOffset;

            this.jumpBackAddressVA = jumpBackAddressVA;
            this.dropOverwrittenInstructions = dropOverwrittenInstructions;
            Console.WriteLine($"X86MidFunctionHook: {addressVA.ToString("X8")}, jumpBackOffset={jumpBackOffset}");
        }

        public Instruction[] GetOverwrittenInstructions()
        {
            return GetInstructionsAtAddress(ref stubAsm, targetAddressVA, (uint)IntPtr.Size + 1);
        }

        /// <summary>
        /// Fix the nextIP in the instructions (useful when moving existing instructions around)
        /// </summary>
        /// <param name="instructions"></param>
        public void AlignInstructionsIPWithStub(Instruction[] instructions)
        {
            uint nextIPUpdated = (uint)stubAddressVA;
            for (int i = 0; i < instructions.Length; i++)
            {
                instructions[i].NextIP32 = nextIPUpdated + (uint)instructions[i].Length;
                nextIPUpdated += (uint)instructions[i].Length;
            }
        }

        /// <summary>
        /// Fix the nextIP in the instructions (useful when moving existing instructions around)
        /// </summary>
        /// <param name="instructions"></param>
        public static void ZeroInstructionsIP(Instruction[] instructions)
        {
            for (int i = 0; i < instructions.Length; i++)
            {
                instructions[i].NextIP32 = 0;
            }
        }

        /// <summary>
        /// Build and apply the hook to make it effective.
        /// </summary>
        /// <exception cref="Exception"></exception>
        public void Build(bool includeJumpBack = true)
        {
            overwrittenInstructions = GetInstructionsAtAddress(ref stubAsm, targetAddressVA, (uint)IntPtr.Size + 1);

            // Fix the nextIP in the overwritten instructions so we can move them properly.
            if (!dropOverwrittenInstructions)
            {
                AlignInstructionsIPWithStub(overwrittenInstructions);
                foreach (var inst in overwrittenInstructions)
                {
                    stubAsm.AddInstruction(inst);
                    Console.WriteLine($"Build.RestoreInst: {inst.ToString()}, nextIP: {inst.NextIP32.ToString("X8")}, length={inst.Length} memoryBase: {inst.MemoryBase.ToString()}, memoryDisp: {inst.MemoryDisplacement32.ToString("X8")}");
                }
            }

            // Request memory space for the asm stub
            int overwrittenInstructionsLength = stubAsm.Instructions.Sum(x => x.Length);
            int minStubSize = (int)((overwrittenInstructionsLength + IntPtr.Size + 1) + stubAsm.Instructions.Count() * IntPtr.Size);
            SharedMemory.TryRequestMemory(out stubAddressVA, (IntPtr)minStubSize, out pageAllocationSegment);

            Hook(includeJumpBack);
        }

        public void Hook(bool includeJumpBack = true)
        {
            // only the original instructions will have a length.
            int overwrittenInstructionsLength = stubAsm.Instructions.Sum(x => x.Length);

            // Create the jmp to the stub from target address
            MakeJump(targetAddressVA, stubAddressVA);

            // Assemble the instructions and write to buffer
            using (MemoryStream ms = new MemoryStream())
            {
                if (!stubAsm.TryAssemble(new StreamCodeWriter(ms), (uint)stubAddressVA, out string? errorMessage, out AssemblerResult assemblerResult))
                    throw new Exception($"Iced assemble failed: {errorMessage}");

                var hProcess = Process.GetCurrentProcess().Handle;
                UInt32 oldProtect = 0;
                if (ms.Length != 0 && !Kernel32.VirtualProtectEx(hProcess, stubAddressVA, (uint)ms.Length, Kernel32.PAGE_EXECUTE_READWRITE, &oldProtect))
                    throw new Exception($"VirtualProtectEx Initial failed: {MarshalFallback.GetLastSystemError()}. hProcess={hProcess}, stubAddressVA={stubAddressVA.ToString("X8")}, msLength={ms.Length.ToString("X8")}");

                Marshal.Copy(ms.GetBuffer(), 0, (IntPtr)stubAddressVA, (int)ms.Length);
                if (ms.Length != 0 && !Kernel32.VirtualProtectEx(hProcess, stubAddressVA, (uint)ms.Length, oldProtect, &oldProtect))
                    throw new Exception($"VirtualProtectEx Restore failed: {MarshalFallback.GetLastSystemError()}. hProcess={hProcess}, stubAddressVA={stubAddressVA.ToString("X8")}, msLength={ms.Length.ToString("X8")}");

                // Generate the jmp back to resume the targets routine.
                if (includeJumpBack)
                {
                    if (jumpBackAddressVA != IntPtr.Zero)
                        MakeJump((IntPtr)(int)stubAddressVA + (int)ms.Length, (IntPtr)jumpBackAddressVA); //MakeJump(stubAddressVA + (IntPtr)ms.Length, (IntPtr)jumpBackAddressVA);
                    else
                        MakeJump((IntPtr)(int)stubAddressVA + (int)ms.Length, (IntPtr)(int)targetAddressVA + IntPtr.Size + 1 + (int)jumpBackOffset); //MakeJump(stubAddressVA + (IntPtr)ms.Length, targetAddressVA + IntPtr.Size + 1 + jumpBackOffset);
                }
            }
        }

        public void UnHook()
        {
            Assembler asm = new Assembler(32);
            foreach (Instruction instruction in overwrittenInstructions!)
            {
                asm.AddInstruction(instruction);
            }

            // Assemble the instructions and write to buffer
            using (MemoryStream ms = new MemoryStream())
            {
                if (!asm.TryAssemble(new StreamCodeWriter(ms), (uint)targetAddressVA, out string? errorMessage, out AssemblerResult assemblerResult))
                    throw new Exception($"Iced assemble failed: {errorMessage}");
                var hProcess = Process.GetCurrentProcess().Handle;

                UInt32 oldProtect = 0;
                if (!Kernel32.VirtualProtectEx(hProcess, targetAddressVA, (uint)ms.Length, Kernel32.PAGE_EXECUTE_READWRITE, &oldProtect))
                    throw new Exception($"VirtualProtectEx failed: {MarshalFallback.GetLastSystemError()}");

                Marshal.Copy(ms.GetBuffer(), 0, (IntPtr)targetAddressVA, (int)ms.Length);
                if (!Kernel32.VirtualProtectEx(hProcess, targetAddressVA, (uint)ms.Length, oldProtect, &oldProtect))
                    throw new Exception($"VirtualProtectEx failed: {MarshalFallback.GetLastSystemError()}");
            }
        }

        public IntPtr GetJumpBackAddress()
        {
            if (jumpBackAddressVA != IntPtr.Zero)
                return jumpBackAddressVA;
            else
                return (IntPtr)(int)targetAddressVA + IntPtr.Size + 1 + (int)jumpBackOffset; // targetAddressVA + IntPtr.Size + 1 + jumpBackOffset;
        }

        /// <summary>
        /// Get the instructions at the specified address until designated length.
        /// TODO: Make this function smarter and add logic that when this would retrieve an "incomplete" instruction
        /// due to the length being too short, it "can" give instructions up to the next one aswell.
        /// </summary>
        /// <param name="asm">The assembler to use</param>
        /// <param name="addressVA">The address to get the instructions from</param>
        /// <param name="length">The maximum length/range to consider when getting the instructions</param>
        /// <returns>Read instructions</returns>
        public static Instruction[] GetInstructionsAtAddress(ref Assembler asm, IntPtr addressVA, uint length, uint additionalAddressSpace = 0x256)
        {
            MemoryCodeReader codeReader = new MemoryCodeReader((byte*)addressVA, (int)(length + additionalAddressSpace));
            Decoder decoder = Decoder.Create(32, codeReader);
            decoder.IP = (ulong)addressVA;
            ulong endIP = decoder.IP + length;

            List<Instruction> instructionsList = new List<Instruction>();
            while (decoder.IP < endIP)
            {
                Instruction inst = decoder.Decode();
                instructionsList.Add(inst);
            }
            return instructionsList.ToArray();
        }
        /*
        public static Instruction[] GetInstructionsAtAddressSafe(ref Assembler asm, IntPtr addressVA, uint length, uint additionalAddressSpace = 0x256)
        {
            MemoryCodeReader codeReader = new MemoryCodeReader((byte*)addressVA, (int)(length + additionalAddressSpace));
            Decoder decoder = Decoder.Create(32, codeReader);
            decoder.IP = (ulong)addressVA;

            // Get the first instruction after "length"
            while (decoder.IP < endIP)
            {
                Instruction inst = decoder.Decode();
                instructionsList.Add(inst);
            }

            ulong endIP = decoder.IP + length;

            List<Instruction> instructionsList = new List<Instruction>();
            while (decoder.IP < endIP)
            {
                Instruction inst = decoder.Decode();
                instructionsList.Add(inst);
            }
            return instructionsList.ToArray();
        }*/

        /// <summary>
        /// Create a jmp at address to the designed target address.
        /// </summary>
        /// <param name="addressVA">The address where to create the jmp</param>
        /// <param name="targetAddressVA">The target address the jmp leads to</param>
        /// <exception cref="Exception"></exception>
        public static void MakeJump(IntPtr addressVA, IntPtr targetAddressVA)
        {
            Assembler asm = new Assembler(32);

            asm.jmp((UInt32)targetAddressVA);

            // Assemble the instructions and write to buffer
            using (MemoryStream ms = new MemoryStream())
            {
                if (!asm.TryAssemble(new StreamCodeWriter(ms), (uint)addressVA, out string? errorMessage, out AssemblerResult assemblerResult))
                    throw new Exception($"Iced assemble failed: {errorMessage}");
                var hProcess = Process.GetCurrentProcess().Handle;

                UInt32 oldProtect = 0;
                if (!Kernel32.VirtualProtectEx(hProcess, addressVA, (uint)ms.Length, Kernel32.PAGE_EXECUTE_READWRITE, &oldProtect))
                    throw new Exception($"VirtualProtectEx failed: {MarshalFallback.GetLastSystemError()}");

                Marshal.Copy(ms.GetBuffer(), 0, (IntPtr)addressVA, (int)ms.Length);
                if (!Kernel32.VirtualProtectEx(hProcess, addressVA, (uint)ms.Length, oldProtect, &oldProtect))
                    throw new Exception($"VirtualProtectEx failed: {MarshalFallback.GetLastSystemError()}");
            }
        }

        /// <summary>
        /// Create a jmp at address to the designed target address in a unsafe way.
        /// </summary>
        /// <param name="addressVA">The address where to create the jmp</param>
        /// <param name="targetAddressVA">The target address the jmp leads to</param>
        /// <param name="overwritenLen">Overwrite any remaining bytes with NOPs</param>
        public static void MakeJumpEx(IntPtr address, IntPtr targetAddress, ulong overwritenLen)
        {
            // calculate the distance between our address and our target location
            // and subtract the 5 bytes, which is the size of the jmp
            // (0xE9 0xAA 0xBB 0xCC 0xDD) = 5 bytes
            UInt32 relativeAddress = (UInt32)((int)targetAddress - (int)address) - (uint)(IntPtr.Size + 1);

            // overwrite the byte at pAddress with the jmp opcode (0xE9)
            *((UInt32*)address) = 0xE9;

            // overwrite the next 4 bytes (which is the size of a DWORD)
            // with the relativeAddress
            *((UInt32*)(address + 0x01)) = relativeAddress;

            // overwrite the remaining bytes with NOPs (0x90)
            for (UInt32 x = 0x05; x < overwritenLen; x++)
            {
                *((UInt32*)address + x) = 0x90;
            }
        }

        public void Dispose()
        {
            pageAllocationSegment.Dispose();
        }

    }
}
