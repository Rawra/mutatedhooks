﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using Iced.Intel;
using MutatedHooks.Utility;
using PInvoke.PolyHook2;
using PInvoke.PolyHook2.Mutated.Utility;
using static Iced.Intel.AssemblerRegisters;

namespace MutatedHooks
{
    /// <summary>
    /// Enum definitions as to who cleans up the stack
    /// caller or callee
    /// </summary>
    public enum MutatedStackCleanup
    {
        None = 0,
        Callee = 1,
        Caller = 2,

        // Alias
        StdcallStyle = 1,
        CdeclStyle = 2,

        // X86_64
        X64FastcallStyle = 2,
        VCVectorcallStyle = 2
    }

    /// <summary>
    /// Default supported call convs
    /// </summary>
    public enum MutatedStandardCallConv
    {
        /// <summary>
        /// Mutated Calling Conventions, standards do not apply.
        /// </summary>
        Mutated = 0,

        /// <summary>
        /// CDecl: Caller cleans up; Arguments right to left on the stack.
        /// Return: EAX (value type), ST0 (float type)
        /// </summary>
        Cdecl = 1,

        /// <summary>
        /// Stdcall(WINAPI): Callee cleans up; Arguments right to left on the stack.
        /// Return: EAX (value type), ST0 (float type)
        /// </summary>
        Stdcall = 2,

        /// <summary>
        /// Fastcall(WINAPI): Callee cleans up; Arguments right to left on the stack; Priority Arg Registers: ECX, EDX.
        /// Return: EAX (value type), ST0 (float type)
        /// </summary>
        Fastcall = 3,

        /// <summary>
        /// VC Thiscall: Callee cleans up; Arguments right to left on the stack; Priority Arg Registers: ECX (implicit "This" ptr).
        /// Return: EAX (value type), ST0 (float type)
        /// 
        /// Note: 
        /// When variable number of args, the Caller is expected to clean up the stack
        /// </summary>
        MicrosoftThiscall = 4,

        /// <summary>
        /// GCC Thiscall: Caller cleans up; Arguments right to left on the stack; First stack argument is the implicit "This" ptr.
        /// Return: EAX (value type), ST0 (float type)
        /// </summary>
        GCCThiscall = 5,

        /// <summary>
        /// X64 Fastcall: Callee cleans up; Integer arguments passed via RCX, RDX, R8, R9; 
        /// Floating-point arguments passed via XMM0–XMM3; remaining arguments on the stack (Right to left).
        /// Return: RAX (value type), XMM0 (float type)
        /// 
        /// Note:
        /// In the Microsoft x64 calling convention, it is the caller's responsibility to allocate 32 bytes 
        /// of "shadow space" on the stack right before calling the function (regardless of the actual number of parameters used), 
        /// and to pop the stack after the call. 
        /// 
        /// The shadow space is used to spill RCX, RDX, R8, and R9, but must be made available to all functions, 
        /// even those with fewer than four parameters.
        /// </summary>
        MicrosoftX64Fastcall = 6,

        /// <summary>
        /// VC Vectorcall: Callee cleans up; Vector arguments (SIMD types) passed via XMM0–XMM5 or YMM0–YMM5; 
        /// Floating-point and __m128[Float-Vector4] types are passed in XMM registers, and __m256[Float-Vector8] types are passed in YMM registers
        /// Integer arguments passed via RCX, RDX, R8, R9; remaining arguments on the stack (Right to left).
        /// Return: RAX (value type), XMM0 (float type)
        /// </summary>
        VCVectorcall = 7
    }

    /// <summary>
    /// Argument representation with specification if its supposed to be
    /// provided over a register
    /// </summary>
    [SkipLocalsInit]
    public sealed class MutatedArgument32
    {
        /// <summary>
        /// The representing argument type
        /// </summary>
        public Type argumentType;

        /// <summary>
        /// The register to be used instead of the argument being on the stack
        /// </summary>
        public AssemblerRegister32? register;

        /// <summary>
        /// Indicates if this is a reference object
        /// and will be marshalled
        /// </summary>
        public bool marshalledReferenceObject = false;

        public MutatedArgument32(Type argumentType, AssemblerRegister32? register = null, bool marshalledReferenceObject = false)
        {
            this.argumentType = argumentType;
            this.register = register;
            this.marshalledReferenceObject = marshalledReferenceObject;
        }

        public MutatedArgument32(AssemblerRegister32 register, bool marshalledReferenceObject = false)
        {
            this.argumentType = typeof(UInt32);
            this.register = register;
            this.marshalledReferenceObject = marshalledReferenceObject;
        }
    }

    /// <summary>
    /// Specialized hook to deal Link-time Code Generation (LTCG) optimized functions resulting in
    /// non standard calling conventions.
    /// 
    /// Features: 
    /// - Cdecl/Stdcall-style stack cleanup
    /// - Custom return register
    /// - Shared VirtualAlloc memory region to store all (per process) stubs
    /// - x86 environments
    /// - All known standard calling conventions for x86
    /// - Function pointer support (delegate is still used to help construct the stubs)
    /// ToDo:
    /// - [DONE] Cdecl-style stack cleanup
    /// - [DONE] Custom return register
    /// - [DONE] Re-use the VirtualAlloc region by keeping track where we are and using a address offset for subsequent usage, and keep track of allocated mem for disposal
    /// - [DONE] Changed the way the target address is stored (from using EAX to using the stack frame, now its stored at ebp - 4)
    /// - [DONE] Simplified usage API
    /// - [DONE] Add ease of access call convs: Fastcall:OK, Thiscall:TODO
    /// - Add x86_64 support
    /// - [DONE] Function pointer support
    /// - [DONE] Hook, UnHook methods
    /// ########################################################
    /// 
    /// </summary>
    /// <typeparam name="TFunction">The delegate this hook would match with</typeparam>
    [SkipLocalsInit]
    public sealed unsafe class X86MutatedHook<TFunction> : IDisposable where TFunction : Delegate
    {

        /// <summary>
        /// Enum definition to make it clear if we are either calling a standard call conv function
        /// or we want to call a non-standard one.
        /// </summary>
        private enum StubArgumentLayoutMode
        {
            ToStandard = 0,
            ToNonStandard = 1
        }

        /// <summary>
        /// Enum definitions to help out at stub creation state
        /// </summary>
        private enum StubType
        {
            HookStub = 0,
            CallStub = 1
        }

        /// <summary>
        /// Function Hook Stub: This function gets called by the non-standard call conv function by a underlying hook
        /// and calls the FunctionCallback C# defined function.
        /// </summary>
        public TFunction? FunctionHookStub;
        public IntPtr functionHookStubAddress;

        /// <summary>
        /// Function Call Stub: This function is a wrapper to call the non-standard call conv from a standard call conv one.
        /// </summary>
        public TFunction? FunctionCallStub;
        public IntPtr functionCallStubAddress;

        /// <summary>
        /// Underlying hook, based on PolyHook2.
        /// </summary>
        public X86ManagedFunctionHook<TFunction>? hook;

        /// <summary>
        /// The function target address (either the hook target, or the call target)
        /// </summary>
        private IntPtr fnTargetAddress;

        /// <summary>
        /// Function callback: This is your C# Hook you define when initializing this class.
        /// </summary>
        private TFunction? ManagedFunctionCallback;
        private IntPtr fnCallbackAddress;

        /// <summary>
        /// The stack cleanup agreement of the function this hook works with.
        /// </summary>
        private MutatedStackCleanup stackCleanup;

        /// <summary>
        /// The arguments to be passed to the function this hook works with.
        /// </summary>
        public readonly MutatedArgument32[] arguments;

        /// <summary>
        /// The return argument passed from the function to us.
        /// </summary>
        public readonly MutatedArgument32? returnArgument;

        /// <summary>
        /// The calling convention this hook uses.
        /// </summary>
        private readonly MutatedStandardCallConv callConv;

        /// <summary>
        /// Setting to either save non volatile registers during calls
        /// or not.
        /// </summary>
        private readonly bool saveNonVolatileRegister = true;

        /// <summary>
        /// This represents this hooks very own Page Allocation Segment
        /// inside the SharedMemory block
        /// </summary>
        private SharedMemory.PageAllocationSegment pageAllocationSegment;

        /// <summary>
        /// V1 API
        /// Specialized hook to deal with Link-time Code generation optimized functions resulting in
        /// non standard calling conventions.
        /// 
        /// Generates two function stubs for each unique function provided to add a level of indirection
        /// to adjust the stack in such a way that allows standard call conv functions
        /// to call into non - standard ones, and vice versa.
        /// 
        /// Comes with a CallStub and a HookStub, where the CallStub is to be used by the net application, while
        /// the hookstub acts as a hook and will call from native code into managed code of our net applícation.
        /// </summary>
        /// <param name="fnAddress">The address of the native function to hook</param>
        /// <param name="fnCallback">The C# function callback this hook responds to</param>
        /// <param name="arguments">The arguments of the function this hook is associated with</param>
        /// <param name="returnArgument">The way the return argument is expected to work for the function</param>
        /// <param name="stackCleanup">Who cleans the stack after the stubs have been executed</param>
        public X86MutatedHook(
            IntPtr fnAddress,
            TFunction? fnCallback,
            MutatedArgument32[] arguments,
            MutatedArgument32 returnArgument = null,
            MutatedStackCleanup stackCleanup = MutatedStackCleanup.StdcallStyle)
        {
            //Console.WriteLine($"V1 CTOR Var1");
            DelegateArgumentsSanityCheck();

            this.arguments = arguments;
            this.returnArgument = returnArgument;
            this.stackCleanup = stackCleanup;
            this.ManagedFunctionCallback = fnCallback;
            this.callConv = MutatedStandardCallConv.Mutated;
            this.fnTargetAddress = fnAddress;

            if (fnCallback != null)
            {
                TryAllocateStub(out functionHookStubAddress, out FunctionHookStub);
                this.hook = new X86ManagedFunctionHook<TFunction>(fnAddress, FunctionHookStub!, true);
                CreateStub(StubType.HookStub, functionHookStubAddress);
            }

            TryAllocateStub(out functionCallStubAddress, out FunctionCallStub);
            CreateStub(StubType.CallStub, functionCallStubAddress);
        }

        /// <summary>
        /// V1 API
        /// This constructor other than the main one is to be used for known but potentially unsupported calling conventions
        /// such as fastcall or others.
        /// </summary>
        /// <param name="fnAddress">The address of the native function to hook</param>
        /// <param name="fnCallback">The C# function callback this hook responds to</param>
        /// <param name="imageBase">The imagebase for the assembling process</param>
        /// <param name="arguments">The arguments of the function this hook is associated with</param>
        /// <param name="callConv">The standard or know calling convention to emulate</param>
        public X86MutatedHook(
            IntPtr fnAddress,
            TFunction fnCallback,
            MutatedArgument32[] arguments,
            MutatedStandardCallConv callConv)
        {
            //Console.WriteLine($"V1 CTOR Var2");
            this.callConv = callConv;

            DelegateArgumentsSanityCheck();
            PrepareArgumentsForCallConv();

            this.arguments = arguments;
            this.ManagedFunctionCallback = fnCallback;
            this.fnTargetAddress = fnAddress;

            if (fnCallback != null)
            {
                TryAllocateStub(out functionHookStubAddress, out FunctionHookStub);
                this.hook = new X86ManagedFunctionHook<TFunction>(fnAddress, FunctionHookStub!, true);
                CreateStub(StubType.HookStub, functionHookStubAddress);
            }

            TryAllocateStub(out functionCallStubAddress, out FunctionCallStub);
            CreateStub(StubType.CallStub, functionCallStubAddress);
        }

        /// <summary>
        /// V2 API
        /// Specialized hook to deal with Link-time Code generation optimized functions resulting in
        /// non standard calling conventions.
        /// 
        /// Generates two function stubs for each unique function provided to add a level of indirection
        /// to adjust the stack in such a way that allows standard call conv functions
        /// to call into non - standard ones, and vice versa.
        /// 
        /// Comes with a CallStub and a HookStub, where the CallStub is to be used by the net application, while
        /// the hookstub acts as a hook and will call from native code into managed code of our net applícation.
        /// </summary>
        /// <param name="fnAddress">The address of the native function to hook</param>
        /// <param name="fnCallback">The C# function callback this hook responds to</param>
        /// <param name="imageBase">The imagebase for the assembling process</param>
        public X86MutatedHook(
            IntPtr fnAddress,
            TFunction fnCallback)
        {
            //Console.WriteLine($"V2 CTOR");
            DelegateArgumentsSanityCheck();

            //this.arguments = arguments
            LoadMutatedArgumentsByDelegate(out this.arguments, out this.returnArgument);
            LoadCallConvAndStackCleanupByDelegate(out this.callConv, out this.stackCleanup);
            PrepareArgumentsForCallConv();
            //this.returnArgument = returnArgument;
            //this.stackCleanup = stackCleanup;


            this.ManagedFunctionCallback = fnCallback;
            //this.callConv = MutatedStandardCallConv.Mutated;
            this.fnTargetAddress = fnAddress;

            if (fnCallback != null)
            {
                //Console.WriteLine($"Creating HookStub");
                TryAllocateStub(out functionHookStubAddress, out FunctionHookStub);
                this.hook = new X86ManagedFunctionHook<TFunction>(fnAddress, FunctionHookStub!, true);
                CreateStub(StubType.HookStub, functionHookStubAddress);
            }

            //Console.WriteLine($"Creating CallStub");
            TryAllocateStub(out functionCallStubAddress, out FunctionCallStub);
            CreateStub(StubType.CallStub, functionCallStubAddress);
        }

        /// <summary>
        /// V2-FP API
        /// Specialized hook to deal with Link-time Code generation optimized functions resulting in
        /// non standard calling conventions.
        /// 
        /// Generates two function stubs for each unique function provided to add a level of indirection
        /// to adjust the stack in such a way that allows standard call conv functions
        /// to call into non - standard ones, and vice versa.
        /// 
        /// Comes with a CallStub and a HookStub, where the CallStub is to be used by the net application, while
        /// the hookstub acts as a hook and will call from native code into managed code of our net applícation
        /// 
        /// Use this constructor if you need/want to use function pointers only. The delegate is only
        /// used to help build the stubs.
        /// </summary>
        /// <param name="fnAddress">The address of the native function to hook</param>
        /// <param name="fnCallback">The C# function callback this hook responds to</param>
        /// <param name="imageBase">The imagebase for the assembling process</param>
        public X86MutatedHook(
            IntPtr fnAddress,
            IntPtr fnCallback,
            IntPtr imageBase)
        {
            //Console.WriteLine($"V2 CTOR");
            DelegateArgumentsSanityCheck();

            LoadMutatedArgumentsByDelegate(out this.arguments, out this.returnArgument);
            LoadCallConvAndStackCleanupByDelegate(out this.callConv, out this.stackCleanup);
            PrepareArgumentsForCallConv();

            this.fnCallbackAddress = fnCallback;
            //this.callConv = MutatedStandardCallConv.Mutated;
            this.fnTargetAddress = fnAddress;

            if (fnCallback != IntPtr.Zero)
            {
                //Console.WriteLine($"Creating HookStub");
                TryAllocateStub(out functionHookStubAddress, out FunctionHookStub);
                this.hook = new X86ManagedFunctionHook<TFunction>(fnAddress, FunctionHookStub!, true);
                CreateStub(StubType.HookStub, functionHookStubAddress);
            }

            //Console.WriteLine($"Creating CallStub");
            TryAllocateStub(out functionCallStubAddress, out FunctionCallStub);
            CreateStub(StubType.CallStub, functionCallStubAddress);
        }

        /// <summary>
        /// Unhook the installed hook.
        /// </summary>
        public void UnHook()
        {
            if (hook != null && hook.IsHooked())
            {
                hook.UnHook();
            }
        }

        /// <summary>
        /// Hook the targetted function
        /// Note: The hook will be active once the constructor of the class finishes if a callback
        /// is specified automatically.
        /// </summary>
        public void Hook()
        {
            if (hook != null && !hook.IsHooked())
            {
                hook.Hook();
            }
        }

        /// <summary>
        /// Get the mutated arguments and return argument by the TFunction generic delegate itself.
        /// </summary>
        /// <param name="mutatedArguments">The mutated arguments read from the delegate</param>
        /// <param name="returnArgument">The mutated return argument read from the delegate</param>
        /// <returns></returns>
        private bool LoadMutatedArgumentsByDelegate(out MutatedArgument32[] mutatedArguments, out MutatedArgument32? returnArgument)
        {
            List<MutatedArgument32> args = new List<MutatedArgument32>();
            //Console.WriteLine($"GetMutatedArgumentsByDelegate");

            MethodInfo methodInfo = typeof(TFunction).GetMethod("Invoke")!;

            // Read the arguments from the generic delegate
            foreach (ParameterInfo? paramInfo in methodInfo.GetParameters())
            {
                AssemblerRegister32? argRegister = null;
                if (Attribute.IsDefined(paramInfo, typeof(MutatedArgument)))
                {
                    //Console.WriteLine($"GetMutatedArgumentsByDelegate: Attribute Is Defined: {y.Name}={y.ParameterType.Name}");
                    argRegister = paramInfo.GetCustomAttribute<MutatedArgument>()!.register32;
                }

                // Make sure if this ia reference object, that it has a MarshalAs attribute and mark it as such if it does.
                bool marshalledReferenceObject = false;
                if (!paramInfo.ParameterType.IsValueType && Attribute.IsDefined(paramInfo, typeof(MarshalAsAttribute)))
                    marshalledReferenceObject = true;

                if (paramInfo.IsOut || paramInfo.ParameterType.IsPointer)
                    args.Add(new MutatedArgument32(typeof(IntPtr), argRegister, marshalledReferenceObject));
                else
                    args.Add(new MutatedArgument32(paramInfo.ParameterType, argRegister, marshalledReferenceObject));
                //Console.WriteLine($"GetMutatedArgumentsByDelegate: {y.Name}, {y.ParameterType.Name}");
            }

            // Read the return argument from the generic delegate
            if (methodInfo.ReturnType != typeof(void))
            {
                AssemblerRegister32? argRegister = null;
                if (Attribute.IsDefined(methodInfo, typeof(MutatedArgument)))
                    argRegister = methodInfo.GetCustomAttribute<MutatedArgument>()!.register32;

                returnArgument = new MutatedArgument32(methodInfo.ReturnType);
                if (!methodInfo.ReturnType.IsValueType && Attribute.IsDefined(methodInfo, typeof(MarshalAsAttribute)))
                    returnArgument.marshalledReferenceObject = true;

            }
            else
                returnArgument = null;

            mutatedArguments = args.ToArray();
            return true;
        }

        /// <summary>
        /// Loads the calling convention and stack cleanup procedure used by the function targeted by this hook.
        /// </summary>
        /// <param name="callConv">The calling convention used</param>
        /// <param name="mutatedStackCleanup">The stack cleanup procedure used</param>
        private void LoadCallConvAndStackCleanupByDelegate(out MutatedStandardCallConv callConv, out MutatedStackCleanup mutatedStackCleanup)
        {
            MutatedFunction attrib = typeof(TFunction).GetCustomAttribute<MutatedFunction>()!;
            callConv = attrib.mutatedStandardCallConv;
            mutatedStackCleanup = attrib.mutatedStackCleanup;

            //Console.WriteLine($"LoadCallConvAndStackCleanupByDelegate: callConv={callConv.ToString()}, mutatedStackCleanup={mutatedStackCleanup.ToString()}");
        }

        /// <summary>
        /// Sets up the arguments and the stak cleanup according to
        /// the currently selected calling convention.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        private void PrepareArgumentsForCallConv()
        {
            // Set up stack cleaning behaviour and register arguments according to call conv
            switch (this.callConv)
            {
                case MutatedStandardCallConv.Cdecl:
                    this.stackCleanup = MutatedStackCleanup.CdeclStyle;
                    //Console.WriteLine("PrepareCallConv: Set as cdecl");
                    break;
                case MutatedStandardCallConv.MicrosoftThiscall:
                    if (this.arguments.Length < 1)
                        throw new ArgumentException($"VCThiscall with lower than 1 arguments is not supported, you need to provide the (this) pointer.");

                    this.arguments[0].register = new AssemblerRegister32(Register.ECX);
                    this.stackCleanup = MutatedStackCleanup.StdcallStyle;
                    //Console.WriteLine("PrepareCallConv: Set as VCThiscall");
                    break;
                case MutatedStandardCallConv.GCCThiscall:
                    this.stackCleanup = MutatedStackCleanup.CdeclStyle;
                    //Console.WriteLine("PrepareCallConv: Set as GCCThisCall");
                    break;
                case MutatedStandardCallConv.Stdcall:
                    this.stackCleanup = MutatedStackCleanup.StdcallStyle;
                    //Console.WriteLine("PrepareCallConv: Set as StdCall");
                    break;
                case MutatedStandardCallConv.Fastcall:
                    if (this.arguments.Length < 2)
                        throw new ArgumentException($"Fastcall with less than 2 arguments is not supported.");

                    this.arguments[0].register = new AssemblerRegister32(Register.ECX);
                    this.arguments[1].register = new AssemblerRegister32(Register.EDX);
                    this.stackCleanup = MutatedStackCleanup.StdcallStyle;
                    //Console.WriteLine("PrepareCallConv: Set as FastCall");
                    break;
            }
        }

        /// <summary>
        /// Ensure the delegate consists only of value types, and if there are reference types, make sure
        /// they are marked as MarshalAs, if not, we have a huge problem.
        /// </summary>
        /// <exception cref="ArgumentException">The reason why the check has failed</exception>
        private void DelegateArgumentsSanityCheck()
        {
            // Check if the delegate type's parameters and return type are value types
            MethodInfo? methodInfo = typeof(TFunction).GetMethod("Invoke");
            if (methodInfo == null)
            {
                throw new ArgumentException("Invalid delegate type.");
            }

            var parameters = methodInfo.GetParameters();
            if (
                !parameters.All(p => p.ParameterType.IsValueType || Attribute.IsDefined(p, typeof(MarshalAsAttribute)) || p.IsOut || p.ParameterType.IsPointer)
                || !(methodInfo.ReturnParameter.ParameterType.IsValueType || Attribute.IsDefined(methodInfo.ReturnParameter, typeof(MarshalAsAttribute)) || methodInfo.ReturnParameter.ParameterType.IsPointer))
            {
                //foreach (ParameterInfo? param in parameters)
                //{
                //    Console.WriteLine($"arg: {param.Name}={param.ParameterType.Name}, isValueType={param.ParameterType.IsValueType}, isOut={param.IsOut}");
                //}
                //Console.WriteLine($"return: {methodInfo.ReturnParameter.Name}={methodInfo.ReturnParameter.ParameterType.Name}");
                throw new ArgumentException($"Delegate parameters and return type must be value types! problematic method: {methodInfo.Name}");
            }

            // Check if the function is marked as MutatedFunction
            if (!Attribute.IsDefined(typeof(TFunction), typeof(MutatedFunction)))
                throw new ArgumentException($"Function delegate is not marked as MutatedFunction: {typeof(TFunction).Name}");
        }

        /// <summary>
        /// Allocate the memory region for a stub function.
        /// </summary>
        /// <param name="stubAddress">The address where the stub has been created</param>
        /// <param name="stubFunction">The stub function delegate</param>
        /// <returns></returns>
        private bool TryAllocateStub(out IntPtr stubAddress, out TFunction? stubFunction)
        {
            uint estimatedSize = 0;
            estimatedSize = GetTotalArgumentsSize(false);

            // Additional register space
            estimatedSize += 7 * (uint)IntPtr.Size; // prologue/epilogue registers used * pointer size

            // EDITED SEGMENT
            // Fix memory alignment 
            if (estimatedSize % 2 == 0)
                estimatedSize++;

            // Check if already have allocated a memory page for ourselves
            uint processId = (uint)Process.GetCurrentProcess().Id;
            /*if (!MutatedHookSharedMemory.allocated.ContainsKey(processId))
            {
                stubAddress = Kernel32.VirtualAlloc(0, MutatedHookSharedMemory.DEFAULT_PAGE_SIZE, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
                FillMemory(stubAddress, MutatedHookSharedMemory.DEFAULT_PAGE_SIZE, 0xCC); // INT3
                if (!MutatedHookSharedMemory.allocated.TryAdd(processId, new AllocationInfoBlock(stubAddress, MutatedHookSharedMemory.DEFAULT_PAGE_SIZE)))
                    Console.WriteLine($"TryAllocateStub: failed to add VirtualAlloc ptr to allocated cache!");
            }
            else
            {
                stubAddress = MutatedHookSharedMemory.allocated[processId].address + MutatedHookSharedMemory.nextAvailableMemoryOffset;
                MutatedHookSharedMemory.allocated[processId].refcount++;
            }
            MutatedHookSharedMemory.nextAvailableMemoryOffset += (IntPtr)estimatedSize;*/
            SharedMemory.TryRequestMemory(out stubAddress, (IntPtr)estimatedSize, out pageAllocationSegment);

            //Console.WriteLine($"TryAllocateStub: stubAddress: {stubAddress.ToString("X8")}");
            //Console.WriteLine($"TryAllocateStub: nextAvailableMemoryOffset: {MutatedHookSharedMemory.nextAvailableMemoryOffset.ToString("X8")}");

            if (stubAddress == IntPtr.Zero)
            {
                Console.WriteLine($"TryAllocateStub failed: {Marshal.GetLastWin32Error()}");
                stubFunction = null;
                return false;
            }
            stubFunction = Marshal.GetDelegateForFunctionPointer<TFunction>(stubAddress);


            //Console.WriteLine($"TryAllocateStub: stub address: {stubAddress.ToString("X8")}, size: {estimatedSize.ToString("X8")}");

            return true;
        }

        /// <summary>
        /// Retrieve the total argument size in bytes, and specify if to ignore
        /// register defined arguments or count them aswell.
        /// </summary>
        /// <param name="ignoreRegisterArgs">Sets if register args will be ignored when counting size</param>
        /// <returns></returns>
        private uint GetTotalArgumentsSize(bool ignoreRegisterArgs = true)
        {
            uint size = 0;
            foreach (var x in arguments)
            {
                if (ignoreRegisterArgs)
                    if (x.register != null)
                        continue;

                size += StructHelper.GetManagedSize(x.argumentType);
                //Console.WriteLine($"type {x.argumentType} size total: {size}");
            }
            return size;
        }

        /// <summary>
        /// Create the function call stub to convert between standard call convs and non standard ones
        /// and vice-versa.
        /// 
        /// Supported stub-types:
        /// CallStub: C# Function -> Function Call Stub (this) [Stdcall] -> Mutated Function
        /// HookStub: Mutated Function -> Function Hook Stub (this) [Stdcall] -> C# Hook
        /// </summary>
        /// <param name="stubType">Define what type of stub to create</param>
        /// <param name="stubDestinationAddressVA">Define where the stub is to be created</param>
        /// <exception cref="Exception"></exception>
        private void CreateStub(StubType stubType, IntPtr stubDestinationAddressVA)
        {
            uint argumentStackSize = 0;
            if (stubType == StubType.CallStub)
                argumentStackSize = GetTotalArgumentsSize(false);
            else
                argumentStackSize = GetTotalArgumentsSize();

            int bitness = IntPtr.Size == 4 ? 32 : 64;
            Assembler asm = new Assembler(bitness);

            // prologue
            // set up the ebp-stackframe
            asm.push(ebp);        // Save old base pointer
            asm.mov(ebp, esp);    // Set up new base pointer

            // push arguments onto stack
            // [ebp + 8] - first argument
            // [ebp + 4] - return address
            // [ebp + 0] - saved stackbase - pointer register
            int startIndex = 0;
            int stackPointerOffset = (int)argumentStackSize + 4;    // I forgot why we needed this 4 actually, but stuff breaks without it. TODO
            //Console.WriteLine($"CreateStub: processing {arguments.Length} args");
            switch (stackCleanup)
            {
                // Iterate over the arguments in reverse order
                // right-to-left style
                case MutatedStackCleanup.StdcallStyle:
                case MutatedStackCleanup.CdeclStyle:
                    for (int i = arguments.Length - 1; i >= startIndex; i--)
                    {
                        //Console.WriteLine($"CreateStub: processing argument: {i}/{arguments.Length - 1}, sp_offset={stackPointerOffset}");

                        if (stubType == StubType.CallStub)
                            PrepareArgumentType(ref asm, arguments[i], ref stackPointerOffset, StubArgumentLayoutMode.ToNonStandard);
                        else
                            PrepareArgumentType(ref asm, arguments[i], ref stackPointerOffset, StubArgumentLayoutMode.ToStandard);
                    }
                    break;
            }

            // create call
            if (stubType == StubType.CallStub)
            {
                // optional hook specified or not (this might be a callstub only instance)
                if (this.hook != null)
                {
                    //Console.WriteLine($"Pushing target of call (hook-mode-callstub): fnTrampline={this.hook.fnTrampolineAddress.ToString("X8")}");
                    asm.call((uint)this.hook.fnTrampolineAddress);
                }
                else
                {
                    //Console.WriteLine($"Pushing target of call (wrapper-mode): functionTargetAddress={this.functionTargetAddress.ToString("X8")}");
                    asm.call((uint)this.fnTargetAddress);
                }
            }
            else
            {
                //Console.WriteLine($"Pushing target of call (hook-mode-hookstub): FunctionCallback");
                // support for a managed callback, or a unmanaged function pointer as callback option.
                if (this.ManagedFunctionCallback != null)
                {
                    asm.call((uint)Marshal.GetFunctionPointerForDelegate<TFunction>(this.ManagedFunctionCallback!));
                }
                else
                {
                    asm.call((uint)this.fnCallbackAddress);
                }
            }

            // epilogue

            // stack cleanup - stack frame
            asm.mov(esp, ebp);  // restore stack pointer to original base pointer
            asm.pop(ebp);       // restore old base pointer

            // adjust return register, when needed
            if (returnArgument != null && returnArgument.register.HasValue)
            {
                switch (stubType)
                {
                    case StubType.CallStub:
                        //Console.WriteLine($"Adjusting return argument for CallStub to {returnArgument.register.Value.ToString()}");
                        asm.mov(eax, returnArgument.register.Value);
                        break;
                    case StubType.HookStub:
                        //Console.WriteLine($"Adjusting return argument for HookStub from {returnArgument.register.Value.ToString()}");
                        asm.mov(returnArgument.register.Value, eax);
                        break;
                }
            }

            // stack cleanup
            switch (stackCleanup)
            {
                case MutatedStackCleanup.StdcallStyle:
                    //Console.WriteLine($"Creating stdcall-cleanup of size {(short)argumentStackSize}");
                    if (argumentStackSize == 0)
                        asm.ret();
                    else
                        asm.ret((short)argumentStackSize);
                    break;
                case MutatedStackCleanup.CdeclStyle:
                    //Console.WriteLine($"Creating cdecl-cleanup of size {(short)GetTotalArgumentsSize()}");
                    if (argumentStackSize == 0)
                        asm.ret();
                    else
                        asm.ret((short)GetTotalArgumentsSize());
                    break;
            }

            // Assemble the instructions and write to buffer
            using (MemoryStream ms = new MemoryStream())
            {
                if (!asm.TryAssemble(new StreamCodeWriter(ms), (uint)stubDestinationAddressVA, out string? errorMessage, out AssemblerResult assemblerResult))
                {
                    Console.WriteLine($"Iced assemble failed: {errorMessage}");
                    throw new Exception($"Iced assemble failed: {errorMessage}");
                }

                Marshal.Copy(ms.GetBuffer(), 0, (IntPtr)stubDestinationAddressVA, (int)ms.Length);
            }

            //Console.WriteLine($"CreateStub: assembled function at: {functionCallStubAddress.ToString("X8")}");
        }

        /// <summary>
        /// Prepare the argument defined in arg and pass it on to the underlying function that pushes
        /// its value to the stack, while adjusting the ebp stack frame pointer.
        /// </summary>
        /// <param name="asm">Assembly builder reference</param>
        /// <param name="arg">Argument to prepare</param>
        /// <param name="stackPointerOffset">ebp Stack pointer offset</param>
        /// <param name="mode">Argument layout</param>
        private void PrepareArgumentType(ref Assembler asm, MutatedArgument32 arg, ref int stackPointerOffset, StubArgumentLayoutMode mode)
        {
            if (!arg.argumentType.IsValueType && !arg.marshalledReferenceObject)
            {
                Console.WriteLine($"PrepareArgument:\tignoring non-value, non-marshalled argument {arg.argumentType.FullName}, sp_offset={stackPointerOffset}");
                return;
            }

            FieldInfo[] fields = arg.argumentType.GetFields(BindingFlags.Public | BindingFlags.Instance);
            FieldInfo field;

            //Console.WriteLine($"..PrepareArgumentType:\ttype {arg.argumentType.FullName} fields: {fields.Length}");
            switch (stackCleanup)
            {
                case MutatedStackCleanup.StdcallStyle:
                case MutatedStackCleanup.CdeclStyle:
                    if (fields.Length == 0)
                    {
                        //Console.WriteLine($"..PrepareArgumentType:\tprocessing value {arg.argumentType.FullName}, sp_offset={stackPointerOffset}");
                        PushValueOnStack(ref asm, null, arg, ref stackPointerOffset, mode);
                        stackPointerOffset -= 4;
                        break;
                    }

                    // Iterate over the fields in reverse order
                    // right-to-left style
                    for (int i = fields.Length - 1; i >= 0; i--)
                    {
                        field = fields[i];
                        //Console.WriteLine($"..PrepareArgumentType:\tprocessing field {i}/{fields.Length - 1}, {field.Name}, sp_offset={stackPointerOffset}");
                        PushValueOnStack(ref asm, field, arg, ref stackPointerOffset, mode);
                        stackPointerOffset -= 4;
                    }
                    break;
            }
        }

        /// <summary>
        /// Pass the arg onto the stack.
        /// </summary>
        /// <param name="asm">Assembly builder reference</param>
        /// <param name="field">The optional associated field to push onto the stack</param>
        /// <param name="arg">The argument to push onto the stack</param>
        /// <param name="stackPointerOffset">ebp Stack pointer offset</param>
        /// <param name="mode">Argument layout</param>
        private void PushValueOnStack(ref Assembler asm, FieldInfo field, MutatedArgument32 arg, ref int stackPointerOffset, StubArgumentLayoutMode mode)
        {
            //Console.WriteLine($"....PushValueOnStack:\tfield {field?.Name} type: {field?.FieldType.FullName}");
            if (arg.register != null)
            {
                //Console.WriteLine($"....PushValueOnStack:\tprocessing register argument {arg.register!.Value.ToString()}, sp_offset={stackPointerOffset}");
                switch (mode)
                {
                    case StubArgumentLayoutMode.ToStandard:
                        asm.push(arg.register.Value);
                        break;
                    case StubArgumentLayoutMode.ToNonStandard:
                        asm.mov(arg.register.Value, __dword_ptr[ebp + stackPointerOffset]);
                        break;
                }
                return;
            }
            //Console.WriteLine($"....PushValueOnStack:\tprocessing stack argument {field?.Name}, sp_offset={stackPointerOffset}");
            asm.push(__dword_ptr[ebp + stackPointerOffset]);
        }

        /// <summary>
        /// Dispose of resources, in particular our own footprint in
        /// the VirtualAlloc region this hook uses.
        /// </summary>
        public void Dispose()
        {
            //uint processId = (uint)Process.GetCurrentProcess().Id;

            /*if (!SharedMemory.pages.TryGetValue(processId, out var allocation))
            {
                Console.WriteLine($"AOBMutatedHook: failed to open allocated memory page cache by process id: {processId}");
                return;
            }
            allocation.Dispose();*/

            pageAllocationSegment.Dispose();


        }
    }
}
