﻿using Iced.Intel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks
{
    [AttributeUsage(AttributeTargets.ReturnValue | AttributeTargets.Parameter | AttributeTargets.GenericParameter)]
    public sealed class MutatedArgument : Attribute
    {
        public AssemblerRegister32 register32;
        public AssemblerRegister64 register64;

        public MutatedArgument(Register register)
        {
            if (register.IsGPR32())
                this.register32 = new AssemblerRegister32(register);
            else
                this.register64 = new AssemblerRegister64(register);
        }
    }
}
