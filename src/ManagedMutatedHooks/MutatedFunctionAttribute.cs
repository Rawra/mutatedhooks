﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks
{
    [AttributeUsage(AttributeTargets.Delegate)]
    public sealed class MutatedFunction : Attribute
    {
        public MutatedStandardCallConv mutatedStandardCallConv;
        public MutatedStackCleanup mutatedStackCleanup;

        public MutatedFunction(MutatedStandardCallConv callConv, MutatedStackCleanup stackCleanup)
        {
            mutatedStandardCallConv = callConv;
            mutatedStackCleanup = stackCleanup;
        }
    }
}
