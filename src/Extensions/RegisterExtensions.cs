﻿using Iced.Intel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks.Extensions
{
    [SkipLocalsInit]
    internal static class RegisterExtensions
    {
        public static bool IsNonVolatile(this Register self)
        {
            switch (self)
            {
                // x86 non-volatile registers
                case Register.BX:
                case Register.BP:
                case Register.SI:
                case Register.DI:
                case Register.EBX:
                case Register.EBP:
                case Register.ESI:
                case Register.EDI:

                // x86_64 non-volatile registers
                case Register.RBX:
                case Register.RBP:
                case Register.R12:
                case Register.R13:
                case Register.R14:
                case Register.R15:
                case Register.RSI:  // In some calling conventions, RSI and RDI are considered non-volatile
                case Register.RDI:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsVolatile(this Register self)
        {
            switch (self)
            {
                // x86 volatile registers
                case Register.AX:
                case Register.CX:
                case Register.DX:
                case Register.EAX:
                case Register.ECX:
                case Register.EDX:

                // x86_64 volatile registers
                case Register.RAX:
                case Register.RCX:
                case Register.RDX:
                case Register.RSI:  // RSI and RDI are volatile in some conventions
                case Register.RDI:
                case Register.R8:
                case Register.R9:
                case Register.R10:
                case Register.R11:
                    return true;
                default:
                    return false;
            }
        }


    }
}
