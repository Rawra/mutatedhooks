﻿using Iced.Intel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks.Extensions
{
    [SkipLocalsInit]
    public static class AssemblerExtensions
    {
        public static void AddInstructions(this Assembler assembler, Instruction[] instructions)
        {
            foreach (Instruction instruction in instructions)
            {
                assembler.AddInstruction(instruction);
            }
        }
    }
}
