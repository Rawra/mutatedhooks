﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks.Extensions
{
    internal static class MarshalFallback
    {
        /// <summary>
        /// .NET 4.8 fallback for GetLastSystemError
        /// </summary>
        /// <returns></returns>
        public static int GetLastSystemError()
        {
#if NET8_0
            return Marshal.GetLastSystemError();
#else
            return Marshal.GetLastWin32Error();
#endif
        }
    }
}
