﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PInvoke.PolyHook2.Mutated.Utility
{
    internal class StructHelper
    {
        // GetManagedSize() returns the size of a structure whose type
        // is 'type', as stored in managed memory. For any reference type
        // this will simply return the size of a pointer (4 or 8).
        public static uint GetManagedSize(Type type)
        {
            // all this just to invoke one opcode with no arguments!
            var method = new DynamicMethod("GetManagedSizeImpl", typeof(uint), new Type[0], typeof(TypeExtensions), false);
            ILGenerator gen = method.GetILGenerator();

            gen.Emit(OpCodes.Sizeof, type);
            gen.Emit(OpCodes.Ret);

            var func = (Func<uint>)method.CreateDelegate(typeof(Func<uint>));
            return checked((uint)func());    
        }
    }
}
