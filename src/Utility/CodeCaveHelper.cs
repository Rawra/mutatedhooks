﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks.Utility
{
    internal unsafe static class CodeCaveHelper
    {

        /// <summary>
        /// Find a given code cave at a given memory region with length.
        /// This function searches from the end to the beginning to utilize code caves at the very end of the
        /// address space first.
        /// </summary>
        /// <param name="memory">The memory region to search</param>
        /// <param name="length">The size of the memory region</param>
        /// <param name="minSize">The minimum size required of the code cave</param>
        /// <param name="codeCaveVA">The found codecave VA</param>
        /// <returns></returns>
        public static bool TryFindCodeCave(byte* memory, uint length, ulong minSize, out IntPtr codeCaveVA)
        {
            codeCaveVA = IntPtr.Zero;
            //Console.WriteLine($"TryFindCodeCave: memory={new IntPtr(memory).ToString("X8")}, length={length}, minSize={minSize}");

            uint contiguousCount = 0;
            for (int i = (int)length - 1; i >= 0; i--)
            {
                if (memory[i] == 0x00) // Check for zero bytes
                {
                    //Console.WriteLine($"[{i}] = {memory[i]}");
                    contiguousCount++;
                    if ((ulong)contiguousCount >= minSize + 1)
                    {
                        codeCaveVA = IntPtr.Add((IntPtr)memory, (int)(i - contiguousCount + 1));
                        //Console.WriteLine($"TryFindCodeCave: memory={new IntPtr(memory).ToString("X8")}, length={length}, minSize={minSize} TRUE, cc={contiguousCount}");
                        return true;
                    }
                }
                else
                {
                    contiguousCount = 0;
                }
            }
            //Console.WriteLine($"TryFindCodeCave: memory={new IntPtr(memory).ToString("X8")}, length={length}, minSize={minSize} FALSE");
            return false;
        }

    }
}
