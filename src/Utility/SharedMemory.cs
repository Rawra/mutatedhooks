﻿using MutatedHooks.PInvoke;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks.Utility
{
    /// <summary>
    /// Contains shared state data between all MutatedHooks
    /// </summary>
    [SkipLocalsInit]
    public unsafe static class SharedMemory
    {
        /// <summary>
        /// Default memory page size (per-process-id) where this class will be storing its stubs.
        /// </summary>
        internal const uint DEFAULT_PAGE_SIZE = 1024 * 8;

        /// <summary>
        /// Allocation lookup dictionary that keeps track of where we have allocated stuff, and how much we have allocated.
        /// </summary>
        internal static ConcurrentDictionary<uint, PageAllocationBlock> pages = new ConcurrentDictionary<uint, PageAllocationBlock>();

        public static bool TryRequestMemory(out IntPtr memoryAddress, IntPtr minSize, out PageAllocationSegment segment)
        {
            segment = null;

            // Check if already have allocated a memory page for ourselves
            uint processId = (uint)Process.GetCurrentProcess().Id;
            if (!pages.ContainsKey(processId))
            {
                if (!pages.TryAdd(processId, new PageAllocationBlock(DEFAULT_PAGE_SIZE)))
                    Console.WriteLine($"TryRequestMemory: failed to add VirtualAlloc ptr to allocated cache!");

                pages[processId].TryRequestSegment((uint)minSize, out PageAllocationSegment segment2);
                memoryAddress = segment2.address;
                Console.WriteLine($"TryRequestMemory: memoryAddress={memoryAddress.ToString("X16")}");
                return true;
            }
            pages[processId].TryRequestSegment((uint)minSize, out segment);
            memoryAddress = segment.address;
            Console.WriteLine($"TryRequestMemory: memoryAddress={memoryAddress.ToString("X16")}");

            return true;
        }

        public static bool TryDisposeMemory()
        {
            // Check if already have allocated a memory page for ourselves
            uint processId = (uint)Process.GetCurrentProcess().Id;
            if (!pages.ContainsKey(processId))
            {
                Console.WriteLine($"TryDisposeMemory: the process does not have any recorded allocations.");
                return false;
            }
            pages[processId].Dispose();

            return true;
        }

        /// <summary>
        /// Fill out a region of memory with a specified value
        /// </summary>
        /// <param name="ptr">The region to write to</param>
        /// <param name="length">Size of the region</param>
        /// <param name="value">The value to pad the area out with</param>
        public static unsafe void FillMemory(IntPtr ptr, uint length, byte value)
        {
            Span<byte> span = new Span<byte>(ptr.ToPointer(), (int)length);
            span.Fill(value);
        }

        /// <summary>
        /// Represents the VirtualAlloc memory space
        /// and how many users still rely on the allocated region.
        /// </summary>
        [SkipLocalsInit]
        public sealed class PageAllocationBlock : IDisposable
        {
            public IntPtr address;
            public uint refcount;
            public uint minSize;
            public List<PageAllocationSegment> segments;
            public IntPtr nextSegmentPointer;

            public PageAllocationBlock(uint minSize)
            {
                this.minSize = minSize;
                this.refcount = 1;
                this.segments = new List<PageAllocationSegment>();
                this.nextSegmentPointer = IntPtr.Zero;
                
                this.address = Kernel32.VirtualAlloc(IntPtr.Zero, minSize, Kernel32.MEM_COMMIT, Kernel32.PAGE_EXECUTE_READWRITE);
                Console.WriteLine($"PageAllocationBlock: new VirtualAlloc, size={minSize}, address={this.address.ToString("X16")}");
                FillMemory(this.address, minSize, 0xCC); // INT3
            }

            /// <summary>
            /// Request a memory segment inside the allocated memory page.
            /// </summary>
            /// <param name="minSize"></param>
            /// <param name="segment"></param>
            /// <returns></returns>
            public bool TryRequestSegment(uint minSize, out PageAllocationSegment segment)
            {
                // try to re-use any pageallocationsegments that currently exist that fit the size
                IEnumerable<PageAllocationSegment> reusableSegments = segments.Where(x => x.size >= minSize && x.disposed);
                if (reusableSegments.Count() > 0)
                {
                    //segment = new PageAllocationSegment(this, reusableSegments.First().address, minSize);
                    segment = reusableSegments.First();
                } 
                else
                {
                    segment = new PageAllocationSegment(this, (IntPtr)((ulong)this.address + (ulong)this.nextSegmentPointer), minSize); // segment = new PageAllocationSegment(this, this.address + this.nextSegmentPointer, minSize);

                    // EDITED SEGMENT
                    //this.nextSegmentPointer = (IntPtr)((ulong)this.nextSegmentPointer + (ulong)minSize); // this.nextSegmentPointer += (IntPtr)minSize;
                    this.nextSegmentPointer = (nint)((((ulong)this.nextSegmentPointer + (ulong)minSize) + segment.size - 1) & ~(segment.size - 1));
                    Console.WriteLine($"nextSegmentPointer: {nextSegmentPointer}");

                    if ((uint)this.nextSegmentPointer > this.minSize)
                        return false;
                }

                return true;
            }

            public void Dispose()
            {
                // Deduct from reference count
                refcount--;

                // If we have no more subscribers in the VirtualAlloc allocated region, we may dispose of it.
                if (refcount <= 0)
                {
                    if (!Kernel32.VirtualFree(address, 0, Kernel32.MEM_RELEASE))
                    {
                        Console.WriteLine($"SharedMemory: failed to free memory address={address.ToString("X16")}, minSize={minSize}, error: {Marshal.GetLastWin32Error()}");
                        return;
                    }

                    foreach (KeyValuePair<uint, PageAllocationBlock> pageAllocationBlock in pages)
                    {
                        if (pageAllocationBlock.Value.address == address)
                        {
                            pages.TryRemove(pageAllocationBlock.Key, out _);
                            break;
                        }
                    }
                    Console.WriteLine($"SharedMemory: disposed allocated memory page: {address.ToString("X16")}");
                }
            }
        }

        /// <summary>
        /// Represents a segment inside the VirtualAlloc page aka PageAllocationBlock
        /// </summary>
        [SkipLocalsInit]
        public sealed class PageAllocationSegment : IDisposable
        {
            public PageAllocationBlock pageAllocationBlock;
            public IntPtr address;
            public uint size;
            public bool disposed;

            public PageAllocationSegment(PageAllocationBlock pageAllocationBlock, IntPtr address, uint size)
            {
                this.pageAllocationBlock = pageAllocationBlock;
                this.address = address;
                this.size = size;
                this.disposed = false;
            }

            public void Dispose()
            {
                disposed = true;
                FillMemory(address, size, 0xCC);
            }
        }

    }
}
