﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MutatedHooks.Utility
{
    public static class ModuleHelper
    {
        /// <summary>
        /// Get the module base address via moduleName
        /// (alternative to using the PDEngine.NT library or traversing the LDR manually; although this
        /// probably also traverses the LDR at some point)
        /// </summary>
        /// <param name="moduleName"></param>
        /// <returns></returns>
        public static bool TryGetModuleBaseAddress(string moduleName, out IntPtr baseAddress)
        {
            baseAddress = IntPtr.Zero;

            Process currentProcess = Process.GetCurrentProcess();
            foreach (ProcessModule module in currentProcess.Modules)
            {
                if (module.ModuleName.Equals(moduleName, StringComparison.OrdinalIgnoreCase))
                {
                    baseAddress = module.BaseAddress;
                    return true;
                }
            }
            return false;
        }
    }
}
