# mutatedhooks


## What it is

This is a library that builds upon the [PInvoke.PolyHook2](https://gitlab.com/Rawra/pinvoke.polyhook2) library that 
focuses on dealing with functions that have mutated calling conventions, either caused by Link-time Code Generation (LTCG) or function level linking optimizations
by the compiler.

What this means, is with this library, you are provided a class that can deal with such functions in a easy to use manner.

## Examples: MutatedHook

Take for example, this function: 
```cs 
float *__usercall GetVelocityOfCarcass@<eax>(int pCarcassData@<eax>, D3DXVECTOR3 *pVec3@<esi>)
```

Normally you'd have to write 2 assembly stubs to properly hook this function. 
One for hooking the function, to make sure all callsites calling this function with its special call conv will be
translated to a normal one such as CDECL or STDCALL on your callback function.

And one for the scenario where you just want to call the function yourself from a standard call conv location.

This library allows you to express such complicated hooks in a easy manner:

```cs
// XMLGenericNode *__userpurge ModuleManager_FindElementByName@<eax>(int a1@<ecx>, XMLFile *xml_file@<edi>, VC08String_t xml_name_tag)

[MutatedFunction(MutatedStandardCallConv.Mutated, MutatedStackCleanup.StdcallStyle)]
[UnmanagedFunctionPointer(CallingConvention.StdCall)]
public delegate BlittablePointer<XMLGenericNode> eng_modulemanager_findelementbyname_delegate([MutatedArgument(Iced.Intel.Register.ECX)] int a1, [MutatedArgument(Iced.Intel.Register.EDI)] BlittablePointer<XMLFile> xmlFile, VC08String s);
public static X86MutatedHookAOB<eng_modulemanager_findelementbyname_delegate> eng_modulemanager_findelementbyname;

// 1st param: 0xDEADBEEF = Function Address
// 2nd param: Callback function (can be delegate or function pointer)
// 3rd param: ImageBase (required for the stub assembling process)
var x = new X86MutatedHook<eng_modulemanager_findelementbyname_delegate>(0xDEADBEEF, eng_modulemanager_findelementbyname_hook_impl, 0x12345678);
```

This is not all this class is capable of, you can also use this to use other standard but not supported call convs such as Fastcall in C#, simply
by setting the MutatedStandardCallConv used in MutatedFunction to Fastcall. You don't have to specify the registers in the parameters then.

## Examples: InlineHook

If by chance, you are more interested in Inline Hooks, you can also achieve this with the X86InlineHook class:

```cs 
eng_distancedetonator_explode_hook = new X86InlineHook(0x64CEB8, out Assembler asm);
asm.push(esi);
asm.push(__dword_ptr[esp + 8]);
asm.call((uint)(delegate* unmanaged[Stdcall]<DistanceDetonator*, Explosion*, void>)&eng_setup_distancedetonator_explosion_asmhook_impl);
eng_distancedetonator_explode_hook.Build();
```

In the above code, we place a jmp redirection at the designated address, that will point to a stub that features user-given assembly code.
This way users may even interop with C# functions, do some calculations with the given data (that the user needs to push themselves) and return from there.

After the use code is executed, the overwritten instructions are placed at the end before returning to the routine just after where the jmp was placed.